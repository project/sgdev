<?php

namespace Drupal\csv_manager\Service;

use Drupal\Core\File\FileSystemInterface;

/**
 * Class CsvManager
 *
 * @package Drupal\csv_manager\Service
 */
class CsvManager implements CsvManagerInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * CsvManager constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   */
  public function __construct(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * @param array $rows
   * @param string $filename
   * @param string $directory
   * @param bool $append
   *
   * @return bool
   */
  public function createCsvFiles(array $rows, string $filename, string $directory, bool $append = FALSE): bool {
    $this->fileSystem->prepareDirectory($directory, [FileSystemInterface::CREATE_DIRECTORY, FileSystemInterface::MODIFY_PERMISSIONS, FileSystemInterface::EXISTS_REPLACE]);
    $filename = $directory . '/' . $filename . '.csv';

    $mode = 'a+';

    if (!$append && file_exists($filename)) {
      $this->fileSystem->delete($filename);
      $mode = 'w+';
    }

    $csv_file = fopen($filename, $mode);
    if ($csv_file) {
      foreach ($rows as $offset => $record) {
        fputcsv($csv_file, $record, ',', '"');
      }
      fclose($csv_file);
      return TRUE;
    }
    return FALSE;
  }

}
