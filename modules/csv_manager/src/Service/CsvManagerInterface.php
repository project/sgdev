<?php

namespace Drupal\csv_manager\Service;


/**
 * Class CsvManager
 *
 * @package Drupal\csv_manager\Service
 */
interface CsvManagerInterface {

  /**
   * @param array $rows
   * @param string $filename
   * @param string $directory
   * @param bool $append
   *
   * @return bool
   */
  public function createCsvFiles(array $rows, string $filename, string $directory, bool $append = FALSE): bool;
}
