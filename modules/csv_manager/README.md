# Example

## Instantiate a csv service

```php
// Batch factory plugin manager
$csvManager = \Drupal::service('sgdev.csv_manager');

```

## Usage

```php
    /**
     * @var \Drupal\csv_manager\Service\CsvManagerInterface $csvManager
     */
    $csvManager->createCsvFiles([$userDataItems], 'fdtest', 'private://import', 1);
```
