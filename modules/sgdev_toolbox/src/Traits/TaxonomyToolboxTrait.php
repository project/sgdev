<?php

namespace Drupal\sgdev_toolbox\Traits;

/**
 * Trait TaxonomyToolboxTrait
 *
 * @package Drupal\sgdev_toolbox\Traits
 */
trait TaxonomyToolboxTrait {

  /**
   * Finds all terms in a given vocabulary ID.
   *
   * @param string $vid
   *   Vocabulary ID to retrieve terms for.
   * @param int $parent
   *   The term ID under which to generate the tree. If 0, generate the tree
   *   for the entire vocabulary.
   * @param null $maxDepth
   *   The number of levels of the tree to return. Leave NULL to return all
   *   levels.
   *
   * @return object[]|\Drupal\taxonomy\TermInterface[]
   *   An array of term objects that are the children of the vocabulary $vid.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllTreeByVocabulary($vid, int $parent = 0, $maxDepth = NULL, $loadEntities = FALSE): array {
    $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $taxonomy = $termStorage->loadTree($vid, $parent, $maxDepth, $loadEntities);

    // Get terms of the passed vid.
    $terms = $termStorage->loadByProperties(['vid' => 'structure']);

    // Init result array.
    $result = [];

    foreach ($taxonomy as $taxo) {
      if ($taxo->depth == 0) {
        $result[$taxo->tid] = $terms[$taxo->tid];
      }
      else {
        foreach ($taxo->parents as $parentId) {
          if ($parentId != 0 && array_key_exists($parentId, $terms)) {
            if (!is_array($terms[$parentId]->children)) {
              $terms[$parentId]->children = [];
            }
            $terms[$parentId]->children[$taxo->tid] = $terms[$taxo->tid];
          }
        }
      }
    }
    return $result;
  }

}
