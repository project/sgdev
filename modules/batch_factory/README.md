# Example

## Instantiate a batch service

```php
// Batch factory plugin manager
$batchPluginManager = \Drupal::service('plugin.manager.batch_factory');
```

## Usage

```php

  // Create default task instance
    $batchPlugin = $batchPluginManager->createInstance('default');

  // Initialise batch
    $batchPlugin->setBatchTitle('My Batch Test');

    // Override plugin class callback functions (optional)
    $batchPlugin->setBatchExecuteCallback('\Drupal\example\Batch\ExampleBatchCallback::execute');
    $batchPlugin->setbatchFinishedCallback('\Drupal\example\Batch\ExampleBatchCallback::finished');

    // Populate operations.
    foreach ($nids as $nid) {
      // First argument is the callback function argument.
      // Second argument can be used to pass a callback function,
      // otherwise the function set with the "setBatchExecuteCallback" or the default plugin static method will be used.
      $batchPlugin->addBatchOperation([$nid]);
    }

    // Set batch (calls batch_set()).
    $batchPlugin->setBatch();

    // Process batch and redirect to desired url.
    return $batchPlugin->processBatch(\Drupal\Core\Url::fromRoute('<front>'));
  }
```
