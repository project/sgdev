<?php

namespace Drupal\batch_factory;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a batch plugin base.
 *
 * @BatchTask(
 *   id = "MyBatch",
 *   name = @Translation("My Batch"),
 * )
 */
class BatchFactoryPluginBase extends PluginBase implements BatchFactoryInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $batchTitle;

  /**
   * @var array
   */
  protected array $batchOperations;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $batchInitMessage;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $batchProgressMessage;

  /**
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  protected TranslatableMarkup $batchErrorMessage;

  /**
   * @var string
   */
  protected string $batchExecuteCallback;

  /**
   * @var string
   */
  protected string $batchFinishedCallback;

  /**
   * BatchFactoryPluginBase constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $this->batchTitle = $this->t('Batch task');
    $this->batchInitMessage = $this->t('Task creating process is starting.');
    $this->batchProgressMessage = $this->t('Processed @current out of @total. Estimated time: @estimate.');
    $this->batchErrorMessage = $this->t('An error occurred during processing');
    $this->batchOperations = [];
    $this->batchExecuteCallback = '\\' . static::class . '::execute';
    $this->batchFinishedCallback = '\\' . static::class . '::finished';
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return \Drupal\batch_factory\BatchFactoryPluginBase|static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @param $params
   * @param $context
   *
   * @return void
   */
  static function execute($params, &$context) {
    self::execute($params, $context);
  }

  /**
   * @param $success
   * @param $results
   * @param $operations
   *
   * @return mixed|void
   */
  static function finished($success, $results, $operations) {
    self::finished($success, $results, $operations);
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchTitle(): TranslatableMarkup {
    return $this->batchTitle;
  }

  /**
   * @param string $batchTitle
   * ex: D8 Batch task'
   *
   * @return void
   */
  public function setBatchTitle(string $batchTitle): void {
    $this->batchTitle = t($batchTitle);
  }

  /**
   * @return array
   */
  public function getBatchOperations(): array {
    return $this->batchOperations;
  }

  /**
   * @param array $arguments
   * @param string|NULL $function
   *
   * @return void
   */
  public function addBatchOperation(array $arguments, string $function = NULL): void {
    $function = !empty($function) ? $function : $this->batchExecuteCallback;
    $this->batchOperations[] = [$function, $arguments];
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchProgressMessage(): TranslatableMarkup {
    return $this->batchProgressMessage;
  }

  /**
   * @param string $batchProgressMessage
   * ex: 'Processed @current out of @total. Estimated time: @estimate.'
   *
   * @return void
   */
  public function setBatchProgressMessage(string $batchProgressMessage): void {
    $this->batchProgressMessage = t($batchProgressMessage);
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchInitMessage(): TranslatableMarkup {
    return $this->batchInitMessage;
  }

  /**
   * @param string $batchInitMessage
   * ex: 'Task creating process is starting.'
   *
   * @return void
   */
  public function setBatchInitMessage(string $batchInitMessage): void {
    $this->batchInitMessage = $this->t($batchInitMessage);
  }

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchErrorMessage(): TranslatableMarkup {
    return $this->batchErrorMessage;
  }

  /**
   * @param string $batchErrorMessage
   * ex: 'An error occurred during processing.'
   *
   * @return void
   */
  public function setBatchErrorMessage(string $batchErrorMessage): void {
    $this->batchErrorMessage = $this->t($batchErrorMessage);
  }

  /**
   * @return string
   */
  public function getbatchFinishedCallback(): string {
    return $this->batchFinishedCallback;
  }

  /**
   * @param string $batchFinishedCallback
   * ex : \Drupal\d8_batch\Form\BatchTask::finishedCallback
   *
   */
  public function setbatchFinishedCallback(string $batchFinishedCallback): void {
    $this->batchFinishedCallback = $batchFinishedCallback;
  }

  /**
   * @return string
   */
  public function getBatchExecuteCallback(): string {
    return $this->batchExecuteCallback;
  }

  /**
   * @param string $batchExecuteCallback
   * ex: 'Drupal\d8_batch\Form\BatchTaskForm::execute'
   */
  public function setBatchExecuteCallback(string $batchExecuteCallback): void {
    $this->batchExecuteCallback = $batchExecuteCallback;
  }

  /**
   * Processes the batch.
   *
   * This function is generally not needed in form submit handlers;
   * Form API takes care of batches that were set during form submission.
   *
   * @param null $redirect
   *   (optional) Either a path or Url object to redirect to when the batch has
   *   finished processing. For example, to redirect users to the home page, use
   *   '<front>'. If you wish to allow standard form API batch handling to occur
   *   and force the user to be redirected to a custom location after the batch
   *   has finished processing, you do not need to use batch_process() and this
   *   parameter. Instead, make the batch 'finished' callback return an instance
   *   of \Symfony\Component\HttpFoundation\RedirectResponse, which will be used
   *   automatically by the standard batch processing pipeline (and which takes
   *   precedence over this parameter). If this parameter is omitted and no
   *   redirect response was returned by the 'finished' callback, the user will
   *   be redirected to the page that started the batch. Any query arguments will
   *   be automatically persisted.
   * @param \Drupal\Core\Url|null $url
   *   (optional) URL of the batch processing page. Should only be used for
   *   separate scripts like update.php.
   * @param null $redirect_callback
   *   (optional) Specify a function to be called to redirect to the progressive
   *   processing page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   *   A redirect response if the batch is progressive. No return value otherwise.
   */
  public function processBatch($redirect = NULL, Url $url = NULL, $redirect_callback = NULL): ?\Symfony\Component\HttpFoundation\RedirectResponse {
    return batch_process($redirect, $url, $redirect_callback);
  }

  /**
   * @return void
   */
  public function setBatch() {
    batch_set($this->getBatch());
  }

  /**
   * @return array
   */
  public function getBatch(): array {
    return [
      'title' => $this->batchTitle,
      'operations' => $this->batchOperations,
      'init_message' => $this->batchInitMessage,
      'progress_message' => $this->batchProgressMessage,
      'error_message' => $this->batchErrorMessage,
      'finished' => $this->batchFinishedCallback,
    ];
  }

  /**
   * @return mixed
   */
  function getName() {
    return $this->getName();
  }
}
