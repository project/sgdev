<?php

namespace Drupal\batch_factory;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Interface BatchFactoryBaseInterface
 *
 * @package Drupal\batch_factory
 */
interface BatchFactoryInterface extends PluginInspectionInterface {

  /**
   * @param $params
   * @param $context
   *
   * @return mixed
   */
  static function execute($params, &$context);

  /**
   * @param $success
   * @param $results
   * @param $operations
   *
   * @return mixed
   */
  static function finished($success, $results, $operations);

  /**
   * @return mixed
   */
  function getName();

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchTitle(): TranslatableMarkup;

  /**
   * @param string $batchTitle
   * ex: D8 Batch task'
   *
   * @return void
   */
  public function setBatchTitle(string $batchTitle): void;

  /**
   * @return array
   */
  public function getBatchOperations(): array;

  /**
   * @param array $arguments
   * @param string|NULL $function
   *
   * @return void
   */
  public function addBatchOperation(array $arguments, string $function = NULL): void;

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchProgressMessage(): TranslatableMarkup;

  /**
   * @param string $batchProgressMessage
   * ex: 'Processed @current out of @total. Estimated time: @estimate.'
   *
   * @return void
   */
  public function setBatchProgressMessage(string $batchProgressMessage): void;

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchInitMessage(): TranslatableMarkup;

  /**
   * @param string $batchInitMessage
   * ex: 'Task creating process is starting.'
   *
   * @return void
   */
  public function setBatchInitMessage(string $batchInitMessage): void;

  /**
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  public function getBatchErrorMessage(): TranslatableMarkup;

  /**
   * @param string $batchErrorMessage
   * ex: 'An error occurred during processing.'
   *
   * @return void
   */
  public function setBatchErrorMessage(string $batchErrorMessage): void;

  /**
   * @return string
   */
  public function getbatchFinishedCallback(): string;

  /**
   * @param string $batchFinishedCallback
   * ex : \Drupal\d8_batch\Form\BatchTask::finishedCallback
   *
   */
  public function setbatchFinishedCallback(string $batchFinishedCallback): void;

  /**
   * @return string
   */
  public function getBatchExecuteCallback(): string;

  /**
   * @param string $batchExecuteCallback
   * ex: 'Drupal\d8_batch\Form\BatchTaskForm::execute'
   */
  public function setBatchExecuteCallback(string $batchExecuteCallback): void;

  /**
   * @param $redirect
   * @param \Drupal\Core\Url|NULL $url
   * @param $redirect_callback
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|null
   */
  public function processBatch($redirect = NULL, Url $url = NULL, $redirect_callback = NULL): ?\Symfony\Component\HttpFoundation\RedirectResponse;

  /**
   * @return void
   */
  public function setBatch();

  /**
   * @return array
   */
  public function getBatch(): array;
}
