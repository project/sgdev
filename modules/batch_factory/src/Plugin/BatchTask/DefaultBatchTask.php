<?php

namespace Drupal\batch_factory\Plugin\BatchTask;

use Drupal\batch_factory\BatchFactoryPluginBase;

/**
 * Provides a 'default' batch task.
 *
 * @BatchTask (
 *   id = "default",
 *   name = @Translation("Default")
 * )
 */
class DefaultBatchTask extends BatchFactoryPluginBase {

  /**
   * @inheritDoc
   */
  public static function execute($params, &$context) {

    $node = \Drupal::entityTypeManager()->getStorage('node')->load($params['nid']);

    $context['message'] = 'Processing : ' . $node->label();
    $context['results'][] = $node->label();
  }

  /**
   * @inheritDoc
   */
  public static function finished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results),
        'One task processed.',
        '@count tasks processed.');
      \Drupal::messenger()->addMessage($message);
    }
    else {
      \Drupal::messenger()->addError(t('Finished with an error.'));
    }
  }

}
