<?php

namespace Drupal\batch_factory\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a batch factory item annotation object.
 *
 * @see \Drupal\batch_factory\BatchFactoryPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class BatchTask extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the batch task.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

}
