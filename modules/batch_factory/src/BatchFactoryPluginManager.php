<?php

namespace Drupal\batch_factory;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class BatchFactoryPluginManager
 *
 * @package Drupal\batch_factory
 */
class BatchFactoryPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BatchTask', $namespaces, $module_handler, 'Drupal\batch_factory\BatchFactoryInterface', 'Drupal\batch_factory\Annotation\BatchTask');
    $this->alterInfo('batch_factory_tasks_info');
    $this->setCacheBackend($cache_backend, 'batch_factory_plugins');
  }

}
