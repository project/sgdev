<?php

namespace Drupal\sgdev_drush\Command;

use Drupal\Core\Update\UpdateHookRegistry;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\Log\Logger;

/**
 * Class InstalledVersion
 *
 * @package Drupal\sgdev_drush\Command
 */
class InstalledVersionCommand extends DrushCommands {

  /**
   * @var \Drupal\Core\Update\UpdateHookRegistry
   */
  protected UpdateHookRegistry $updateHookRegistry;

  /**
   * InstalledVersionCommand constructor.
   *
   * @param \Drupal\Core\Update\UpdateHookRegistry $updateHookRegistry
   */
  public function __construct(UpdateHookRegistry $updateHookRegistry) {
    parent::__construct();
    $this->updateHookRegistry = $updateHookRegistry;
  }

  /**
   * Set hook update version
   *
   * @command installed-version:set
   * @aliases iv:set,
   * @usage drush installed-version:set "module_name" "version"
   * @usage drush iv:set "module_name" "version"
   *
   */
  public function setVersion(string $moduleName, int $version) {
    $this->updateHookRegistry->setInstalledVersion($moduleName, $version);
  }

  /**
   * Get hook update version
   *
   * @command installed-version:get
   * @aliases iv:get,
   * @usage drush installed-version:get "module_name"
   * @usage drush iv:get "module_name"
   *
   */
  public function getVersion($moduleName) {
    $this->logger()->notice($this->updateHookRegistry->getInstalledVersion($moduleName));
  }


}