# Example

## Parser plugin

```php
// Parser plugin manager
$parserPluginManager = \Drupal::service('plugin.manager.stream_parser');
```

## CSV

```php
    // File uri.
    $uri = '/list.csv';

    // Create csv parser instance
    $parserPlugin = $manager->createInstance('csv');

    // Read
    $parsedCsvArray = $parserPlugin->getData($uri);
```

## XML

```php
    // File uri.
    $uri = '/list.xml';

    // Create csv parser instance
    $parserPlugin = $manager->createInstance('csv');

    // Read
    $parsedXmlArray = $parserPlugin->getData($uri);
```

## RSS

```php
    // URL.
    $url = 'https://madeinfoot.ouest-france.fr/flux/rss_ligue.php?id=16';

    // Create csv parser instance
    $parserPlugin = $manager->createInstance('rss');

    // Read
    $parsedRssArray = $parserPlugin->getData($uri);
```
## Custom plugin
You can create you own parser plugin by extending StreamParserPluginBase class.
Plugin namespace : "Plugin/StreamParser"
