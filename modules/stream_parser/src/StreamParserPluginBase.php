<?php

namespace Drupal\stream_parser;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a stream parser plugin base.
 *
 * @StreamParser(
 *   id = "MyParser",
 *   name = @Translation("My Parser"),
 *   // An array of free options
 *   options={}
 * )
 */
abstract class StreamParserPluginBase extends PluginBase implements StreamParserInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapper;

  /**
   * ParserPluginBase constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapper
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FileSystemInterface $fileSystem, StreamWrapperManagerInterface $streamWrapper) {
    $this->fileSystem = $fileSystem;
    $this->streamWrapper = $streamWrapper;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return \Drupal\stream_parser\StreamParserPluginBase|static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('file_system'), $container->get('stream_wrapper_manager'),);
  }

  /**
   * @param string $uri
   *
   * @return mixed
   * @throws \Exception
   */
  public function getData(string $uri) {
    return $this->prepare($this->read($uri));
  }

  /**
   * @param $uri
   *
   * @return mixed
   * @throws \Exception
   */
  protected function read($uri) {
    if ($this->streamWrapper->isValidUri($uri)) {
      $realPath = $this->fileSystem->realpath($uri);
      return $this->fetch($realPath);
    }
    else {
      try {
        return $this->fetch($uri);
      }
      catch (\Exception $e) {
        throw new \Exception($e->getMessage());
      }
    }
  }

  /**
   * @param $path
   *
   * @return mixed
   */
  public function fetch($path) {
    return $this->fetch($path);
  }

  /**
   * @return mixed
   */
  public function getName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * @return mixed
   */
  public function getOptions() {
    return $this->pluginDefinition['options'];
  }

  public function write() {
    // TODO: Implement write() method.
  }

}
