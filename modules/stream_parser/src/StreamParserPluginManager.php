<?php

namespace Drupal\stream_parser;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class StreamParserPluginManager
 *
 * @package Drupal\stream_parser
 */
class StreamParserPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/StreamParser', $namespaces, $module_handler, 'Drupal\stream_parser\StreamParserInterface', 'Drupal\stream_parser\Annotation\StreamParser');
    $this->alterInfo('stream_parser_parsers_info');
    $this->setCacheBackend($cache_backend, 'stream_parser_plugins');
  }

}
