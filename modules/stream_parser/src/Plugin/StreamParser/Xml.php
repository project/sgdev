<?php

namespace Drupal\stream_parser\Plugin\StreamParser;

use Drupal\stream_parser\StreamParserPluginBase;

/**
 * Provides a 'xml' parser.
 *
 * @StreamParser(
 *   id = "xml",
 *   name = @Translation("XML"),
 *   options={}
 * )
 */
class Xml extends StreamParserPluginBase {

  /**
   * @param $data
   *
   * @return mixed
   */
  public function prepare($data) {
    $json = json_encode($data);
    return json_decode($json, TRUE);
  }

  /**
   * @param $path
   *
   * @return false|mixed|\SimpleXMLElement
   */
  function fetch($path) {
    return simplexml_load_string(file_get_contents($path), "SimpleXMLElement", LIBXML_NOCDATA);
  }
}
