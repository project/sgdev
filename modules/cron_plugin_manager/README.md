# Example

## Cron plugin

```php
// First create a cron plugin extending CronManagerPluginBase

// Instantiate a new cron plugin manager
$cronPluginManager = \Drupal::service('plugin.manager.cron_plugin_manager');
$cronPluginManagerInstance = $cronPluginManager->createInstance('cron_plugin_id');
$cronPluginManagerInstance->needsToRun();
```
