<?php

namespace Drupal\cron_plugin_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Cron item annotation object.
 *
 * @see \Drupal\cron_plugin_manager\CronPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class Cron extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The plugin name
   *
   * @var string
   */
  public string $name;

  /**
   * The plugin state storage id
   *
   * @var string
   */
  public string $stateId;

  /**
   * The plugin cron interval
   *
   * @var string
   */
  public string $interval;

  /**
   * The plugin cron preferred run hour
   *
   * @var string
   */
  public string $preferredHour;

}
