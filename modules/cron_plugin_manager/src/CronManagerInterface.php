<?php

namespace Drupal\cron_plugin_manager;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface CronManagerInterface
 *
 * @package Drupal\cron_plugin_manager
 */
interface CronManagerInterface extends PluginInspectionInterface {

  /**
   * @return string
   */
  function getName(): string;

  /**
   * @return string
   */
  function getStateId(): string;

  /**
   * @return string
   */
  function getInterval(): string;

  /**
   * @return string
   */
  function getPreferredHour(): string;

  /**
   * @return bool
   */
  function needsToRun(): bool;
}
