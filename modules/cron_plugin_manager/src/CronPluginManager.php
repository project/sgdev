<?php

namespace Drupal\cron_plugin_manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class CronPluginManager
 *
 * @package Drupal\cron_plugin_manager
 */
class CronPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Cron', $namespaces, $module_handler, 'Drupal\cron_plugin_manager\CronManagerInterface', 'Drupal\cron_plugin_manager\Annotation\Cron');
    $this->alterInfo('cron_manager_info');
    $this->setCacheBackend($cache_backend, 'cron_manager_plugins');
  }

}
