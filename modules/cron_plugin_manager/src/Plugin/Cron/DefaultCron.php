<?php

namespace Drupal\cron_plugin_manager\Plugin\Cron;

use Drupal\cron_plugin_manager\CronManagerPluginBase;

/**
 * Provides a 'default' cron plugin base.
 *
 * @Cron(
 *   id = "default",
 *   state_id = "cron_manager.default",
 *   interval = "86400",
 *   preferred_hour = "00",
 *   name = @Translation("My Cron"),
 * )
 */
class DefaultCron extends CronManagerPluginBase {

}
