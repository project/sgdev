<?php

namespace Drupal\import_manager;

/**
 * Interface for import_runner plugins.
 */
interface ImportRunnerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
