<?php

namespace Drupal\import_manager\Service;

use Drupal\batch_factory\BatchFactoryPluginBase;
use Drupal\Core\CronInterface;
use Drupal\migrate_manager\Service\MigrateManager;

/**
 * Interface ImportManagerInterface
 *
 * @package Drupal\import_manager\Service
 */
interface ImportManagerInterface {

  /**
   * @return \Drupal\migrate_manager\Service\MigrateManager
   */
  public function getMigrationManager(): MigrateManager;

  /**
   * @param array $migrateIds
   *
   * @return void
   */
  public function setMigrationIds(array $migrateIds);

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setSourceBatchPlugin($pluginId);

  /**
   * @return \Drupal\batch_factory\BatchFactoryPluginBase|null
   */
  public function getSourceBatchPlugin(): ?BatchFactoryPluginBase;

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setMigrationBatchPlugin($pluginId);

  /**
   * @return \Drupal\Core\CronInterface
   */
  public function getCronPlugin(): CronInterface;

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setCronPlugin($pluginId);

  /**
   * @return \Drupal\batch_factory\BatchFactoryPluginBase
   */
  public function getMigrationBatchPlugin(): BatchFactoryPluginBase;

  /**
   * @param $data
   * @param $filename
   * @param $directory
   *
   * @return bool
   */
  public function prepareDataSource($data, $filename, $directory): bool;

  /**
   * @param $data
   * @param $filename
   * @param $directory
   * @param bool $processBatch
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void|null
   */
  public function prepareBatchDataSource($data, $filename, $directory, bool $processBatch = TRUE);

  /**
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   *
   * @return bool
   */
  public function runMigration(bool $update = TRUE, bool $sync = TRUE, int $limit = 0): bool;

  /**
   * @param bool $update
   * @param bool $sync
   * @param bool $processBatch
   * @param int $limit
   *
   * @return mixed
   */
  public function runBatchMigration(bool $update = TRUE, bool $sync = TRUE, bool $processBatch = TRUE, int $limit = 0);
}
