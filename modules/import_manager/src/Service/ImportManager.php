<?php

namespace Drupal\import_manager\Service;

use Drupal\batch_factory\BatchFactoryPluginBase;
use Drupal\batch_factory\BatchFactoryPluginManager;
use Drupal\Core\CronInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\cron_plugin_manager\CronManagerPluginBase;
use Drupal\cron_plugin_manager\CronPluginManager;
use Drupal\csv_manager\Service\CsvManagerInterface;
use Drupal\migrate_manager\Service\MigrateManager;
use Drupal\migrate_manager\Service\MigrateManagerInterface;

/**
 * Class ImportManagerBase
 *
 * @package Drupal\import_manager\Service
 */
class ImportManager implements ImportManagerInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * @var \Drupal\migrate_manager\Service\MigrateManagerInterface
   */
  protected MigrateManagerInterface $migrateManager;

  /**
   * @var \Drupal\csv_manager\Service\CsvManagerInterface
   */
  protected CsvManagerInterface $csvManager;

  /**
   * @var \Drupal\cron_plugin_manager\CronPluginManager
   */
  protected CronPluginManager $cronManager;

  /**
   * @var \Drupal\cron_plugin_manager\CronManagerPluginBase | NULL
   */
  protected ?CronManagerPluginBase $cronInstance;

  /**
   * @var \Drupal\batch_factory\BatchFactoryPluginManager
   */
  protected BatchFactoryPluginManager $batchFactory;

  /**
   * @var \Drupal\batch_factory\BatchFactoryPluginBase | NULL
   */
  protected ?BatchFactoryPluginBase $sourceBatchInstance;

  /**
   * @var \Drupal\batch_factory\BatchFactoryPluginBase
   */
  protected BatchFactoryPluginBase $migrateBatchInstance;

  /**
   * ImportManagerBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   * @param \Drupal\migrate_manager\Service\MigrateManagerInterface $migrateManager
   * @param \Drupal\csv_manager\Service\CsvManagerInterface $csvManager
   * @param \Drupal\batch_factory\BatchFactoryPluginManager $batchFactory
   * @param \Drupal\cron_plugin_manager\CronPluginManager $cronPluginManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelInterface $loggerChannel, MigrateManagerInterface $migrateManager, CsvManagerInterface $csvManager, BatchFactoryPluginManager $batchFactory, CronPluginManager $cronPluginManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannel;
    $this->migrateManager = $migrateManager;
    $this->batchFactory = $batchFactory;
    $this->csvManager = $csvManager;
    $this->cronManager = $cronPluginManager;
  }

  /**
   * @return \Drupal\migrate_manager\Service\MigrateManager
   */
  public function getMigrationManager(): MigrateManager {
    return $this->migrateManager;
  }

  /**
   * @param array $migrateIds
   *
   * @return void
   */
  public function setMigrationIds(array $migrateIds) {
    $this->migrateManager->setMigrationIds($migrateIds);
  }

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setSourceBatchPlugin($pluginId) {
    $this->sourceBatchInstance = !empty($pluginId) ? $this->batchFactory->createInstance($pluginId) : NULL;
  }

  /**
   * @return \Drupal\batch_factory\BatchFactoryPluginBase|null
   */
  public function getSourceBatchPlugin(): ?BatchFactoryPluginBase {
    return $this->sourceBatchInstance;
  }

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setMigrationBatchPlugin($pluginId) {
    $this->migrateBatchInstance = !empty($pluginId) ? $this->batchFactory->createInstance($pluginId) : NULL;
  }

  /**
   * @return \Drupal\Core\CronInterface
   */
  public function getCronPlugin(): CronInterface {
    return $this->cronInstance;
  }

  /**
   * @param $pluginId
   *
   * @return void
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function setCronPlugin($pluginId) {
    $this->cronInstance = !empty($pluginId) ? $this->cronManager->createInstance($pluginId) : NULL;
  }

  /**
   * @return \Drupal\batch_factory\BatchFactoryPluginBase
   */
  public function getMigrationBatchPlugin(): BatchFactoryPluginBase {
    return $this->migrateBatchInstance;
  }

  /**
   * @param $data
   * @param $filename
   * @param $directory
   *
   * @return bool
   */
  public function prepareDataSource($data, $filename, $directory): bool {
    return $this->csvManager->createCsvFiles($data, $filename, $directory);
  }

  /**
   * @param $data
   * @param $filename
   * @param $directory
   * @param bool $processBatch
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void|null
   */
  public function prepareBatchDataSource($data, $filename, $directory, bool $processBatch = TRUE) {

    $this->sourceBatchInstance->setBatchTitle($this->sourceBatchInstance->getPluginId());

    // First operation must clear file.
    $this->sourceBatchInstance->addBatchOperation([
      [
        'row' => $data[0],
        'filename' => $filename,
        'directory' => $directory,
        'append' => FALSE,
      ],
    ]);

    array_shift($data);

    foreach ($data as $row) {
      $this->sourceBatchInstance->addBatchOperation([
        [
          'row' => $row,
          'filename' => $filename,
          'directory' => $directory,
          'append' => TRUE,
        ],
      ]);
    }

    $this->sourceBatchInstance->setBatch();
    if ($processBatch) {
      return $this->sourceBatchInstance->processBatch(\Drupal\Core\Url::fromRoute('<front>'));
    }
  }

  /**
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   *
   * @return bool
   */
  public function runMigration(bool $update = TRUE, bool $sync = TRUE, int $limit = 0): bool {
    return $this->migrateManager->process($update, $sync, $limit);
  }

  /**
   * @param bool $update
   * @param bool $sync
   * @param bool $processBatch
   * @param int $limit
   * @param bool $batch
   *
   * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse|void|null
   */
  public function runBatchMigration(bool $update = TRUE, bool $sync = TRUE, bool $processBatch = TRUE, int $limit = 0, bool $batch = TRUE) {

    $this->migrateBatchInstance->setBatchTitle($this->migrateBatchInstance->getPluginId());

    foreach ($this->migrateManager->getMigrationIds() as $migrationId) {
      $this->migrateBatchInstance->addBatchOperation([
        [
          'migrateIds' => [$migrationId],
          'update' => $update,
          'sync' => $sync,
          'limit' => $limit,
          'batch' => $batch
        ],
      ]);
    }

    $this->migrateBatchInstance->setBatch();
    if ($processBatch) {
      return $this->migrateBatchInstance->processBatch(\Drupal\Core\Url::fromRoute('<front>'));
    }
  }

  /**
   * @return bool
   * @throws \Exception
   */
  public function isCronEligibleToRun(): bool {
    return empty($this->cronInstance) || $this->cronInstance->needsToRun();
  }

}
