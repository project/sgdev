<?php

namespace Drupal\import_manager\Plugin\BatchTask;

/**
 * Provides a 'migrate manager source' batch task.
 *
 * @BatchTask (
 *   id = "prepareEventDataSource",
 *   name = @Translation("Prepare Event Data Source")
 * )
 */
class EventDataSourceBatchTask extends DataSourceBatchTaskBase {

}
