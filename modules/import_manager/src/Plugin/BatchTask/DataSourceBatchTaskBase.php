<?php

namespace Drupal\import_manager\Plugin\BatchTask;

use Drupal\batch_factory\BatchFactoryPluginBase;

/**
 * Provides a base 'migrate manager source' batch task.
 *
 * @BatchTask (
 *   id = "prepareBaseDataSource",
 *   name = @Translation("Prepare Base Data Source")
 * )
 */
abstract class DataSourceBatchTaskBase extends BatchFactoryPluginBase {

  /**
   * @inheritDoc
   */
  public static function execute($params, &$context) {
    \Drupal::service('sgdev.csv_manager')->createCsvFiles([$params['row']], $params['filename'], $params['directory'], $params['append']);
    $context['message'] = 'Processing : ' . $params['row'][0];
    $context['results'][] = $params['row'][0];
  }

  /**
   * @inheritDoc
   */
  public static function finished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results),
        'One task processed.',
        '@count tasks processed.');
      \Drupal::messenger()->addMessage($message);
    }
    else {
      \Drupal::messenger()->addError(t('Finished with an error.'));
    }
  }

}
