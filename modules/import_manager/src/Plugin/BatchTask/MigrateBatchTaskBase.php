<?php

namespace Drupal\import_manager\Plugin\BatchTask;

use Drupal\batch_factory\BatchFactoryPluginBase;

/**
 * Provides a base 'migrate manager Migrate' batch task.
 *
 * @BatchTask (
 *   id = "baseMigration",
 *   name = @Translation("Base Migrate")
 * )
 */
abstract class MigrateBatchTaskBase extends BatchFactoryPluginBase {

  /**
   * @inheritDoc
   */
  public static function execute($params, &$context) {
    /**
     * @var \Drupal\migrate_manager\Service\MigrateManager $migrateManager
     */
    $migrateManager = \Drupal::service('sgdev.migrate_manager');
    $migrateManager->setMigrationIds($params['migrateIds']);
    $migrateManager->process($params['update'], $params['sync'], $params['limit'], $params['batch']);
    $migrateManager->getMigrationsLabel();
    $context['message'] = 'Processing : ' . $migrateManager->getMigrationsLabel()[reset($params['migrateIds'])];
    $context['results'][] = 'Finished';
  }

  /**
   * @inheritDoc
   */
  public static function finished($success, $results, $operations) {
    if ($success) {
      $message = \Drupal::translation()->formatPlural(count($results),
        'One task processed.',
        '@count tasks processed.');
      \Drupal::messenger()->addMessage($message);
    }
    else {
      \Drupal::messenger()->addError(t('Finished with an error.'));
    }
  }

}
