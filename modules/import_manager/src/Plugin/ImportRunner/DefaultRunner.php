<?php

namespace Drupal\import_manager\Plugin\ImportRunner;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\import_manager\ImportRunnerPluginBase;

/**
 * Plugin implementation of the import_runner.
 *
 * @ImportRunner(
 *   id = "default",
 *   label = @Translation("Default"),
 *   description = @Translation("Default description."),
 *   sourceBatchId = "prepareDefaultDataSource",
 *   migrationBatchId = "defaultMigration",
 *   cronId = "",
 *   migrateIds = {
 *       "default_article_node_csv_import",
 *       "default_article_node_csv_import"
 *   },
 *   filename = "default_article",
 *   directory = "private://import/",
 * )
 */
class DefaultRunner extends ImportRunnerPluginBase implements ContainerFactoryPluginInterface {

}
