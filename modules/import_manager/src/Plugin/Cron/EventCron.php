<?php

namespace Drupal\cron_plugin_manager\Plugin\Cron;

use Drupal\cron_plugin_manager\CronManagerPluginBase;

/**
 * Provides a 'event' cron plugin base.
 *
 * @Cron(
 *   id = "event",
 *   state_id = "cron_manager.event",
 *   interval = "86400",
 *   preferred_hour = "",
 *   name = @Translation("Event"),
 * )
 */
class EventCron extends CronManagerPluginBase {

}
