<?php

namespace Drupal\import_manager;

use Drupal\Component\Plugin\PluginBase;
use Drupal\import_manager\Service\ImportManager;
use Drupal\import_manager\Service\ImportManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for import_runner plugins.
 *
 * @ImportRunner(
 *   id = "exemple",
 *   label = @Translation("Exemple"),
 *   description = @Translation("Exemple description."),
 *   sourceBatchId = "prepareExempleDataSource",
 *   migrationBatchId = "exempleMigration",
 *   cronId = "exempleCon",
 *   migrateIds = {
 *       "test_exemple_term_csv_import",
 *       "test_exmple_node_csv_import"
 *   },
 *   filename = "test_article",
 *   directory = "private://import/",
 * )
 */
abstract class ImportRunnerPluginBase extends PluginBase implements ImportRunnerInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * @var \Drupal\import_manager\Service\ImportManager|void
   */
  protected ImportManager $importManager;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   *
   * @return static
   * @throws \Exception
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($container, $configuration, $plugin_id, $plugin_definition, $container->get('sgdev.import_manager.manager'));
  }

  /**
   * ImportRunnerPluginBase constructor.
   *
   * @param $container
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\import_manager\Service\ImportManagerInterface $importManager
   *
   * @throws \Exception
   */
  public function __construct($container, array $configuration, $plugin_id, $plugin_definition, ImportManagerInterface $importManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->importManager = $this->init($importManager);
  }

  /**
   * @param $importManager
   *
   * @return mixed
   * @throws \Exception
   */
  protected function init($importManager) {

    $pluginDefinition = $this->getPluginDefinition();

    foreach (static::MANDATORY_PLUGINS as $plugin) {
      if (empty($pluginDefinition[$plugin])) {
        throw new \Exception(t('Property' . $plugin . 'is missing'));
      }
    }

    // Set source Batch plugin id
    $importManager->setSourceBatchPlugin($pluginDefinition['sourceBatchId']);

    // Set migration Batch plugin id
    $importManager->setMigrationBatchPlugin($pluginDefinition['migrationBatchId']);

    // Set Migrate config ids
    $importManager->setMigrationIds($pluginDefinition['migrateIds']);

    // Set Cron plugin id
    $importManager->setCronPlugin($pluginDefinition['cronId']);

    return $importManager;
  }

  /**
   * @param bool $update
   * @param bool $sync
   *
   * @return bool
   */
  public function runMigration(bool $update = TRUE, bool $sync = TRUE): bool {
    return $this->importManager->runMigration($update, $sync);
  }

  /**
   * @param bool $update
   * @param bool $sync
   * @param bool $processBatch
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void|null
   */
  public function runBatchMigration(bool $update = TRUE, bool $sync = TRUE, bool $processBatch = TRUE) {
    return $this->importManager->runBatchMigration($update, $sync, $processBatch);
  }

  /**
   * @param $data
   * @param $filename
   * @param $directory
   * @param bool $processBatch
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|void|null
   */
  public function prepareBatchDataSource($data, $filename = NULL, $directory = NULL, bool $processBatch = TRUE) {
    $pluginDefinition = $this->getPluginDefinition();
    $filename = !empty($filename) ? $filename : $pluginDefinition['filename'];
    $directory = !empty($directory) ? $directory : $pluginDefinition['directory'];
    return $this->importManager->prepareBatchDataSource($data, $filename, $directory, $processBatch);
  }

  /**
   * @param $data
   * @param $filename
   * @param $directory
   *
   * @return bool
   */
  public function prepareDataSource($data, $filename = NULL, $directory = NULL): bool {
    $pluginDefinition = $this->getPluginDefinition();
    $filename = !empty($filename) ? $filename : $pluginDefinition['filename'];
    $directory = !empty($directory) ? $directory : $pluginDefinition['directory'];
    return $this->importManager->prepareDataSource($data, $filename, $directory);
  }

  /**
   * @return bool
   * @throws \Exception
   */
  public function isCronEligibleToRun(): bool {
    return $this->importManager->isCronEligibleToRun();
  }

}
