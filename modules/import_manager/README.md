# Example

## Usage

```php

  /* Fetch data source */
    $source = simplexml_load_string(file_get_contents('https://madeinfoot.ouest-france.fr/flux/rss_ligue.php?id=16'));

    // Arrange data.
    foreach ($source->channel->item as $item) {
      $items[] = [$item->title->__toString(), $item->description->__toString()];
    }

    /* 1 - Method using Import Runner plugin */
    /** @var Drupal\import_manager\ImportRunnerPluginBase $importRunnerInstance */
    $importRunnerInstance = Drupal::service('plugin.manager.import_runner')->createInstance('event');

    if ($importRunnerInstance->isCronEligibleToRun()) {
      $importRunnerInstance->prepareBatchDataSource($items);

      // Starts migration
      $importRunnerInstance->runMigration();
      // Or starts batch migration
      return $importRunnerInstance->runBatchMigration();
    }

    /* 2 - Method using Import manager */
    /** @var \Drupal\import_manager\Service\ImportManager $importManager */
    $importManager = Drupal::service('sgdev.import_manager.manager');

    // Define source batch plugin id (BatchFactoryPluginBase)
    $importManager->setSourceBatchPlugin('prepareEventDataSource');

    // Process source csv file
    $importManager->prepareBatchDataSource($items, 'test_article', 'private://import/');

    // Define migrate config id
    $importManager->setMigrationIds(['test_article_node_csv_import']);

    // Define migration batch plugin id
    $importManager->setMigrationBatchPlugin('eventMigration');

    if ($importManager->isCronEligibleToRun()) {
      // Process source csv file
      $importManager->prepareBatchDataSource($items, 'test_article', 'private://import/');
      return $importManager->runBatchMigration();
    }
```
