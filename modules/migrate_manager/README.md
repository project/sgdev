# Example

## Instantiate a migrate service

```php
// Migrate manager
$migrateManager = \Drupal::service('sgdev.migrate_manager');

```

## Usage

```php
    /**
     * @var $migrateManager Drupal\migrate_manager\Service\ImportManager
     */
    $migrateManager = Drupal::service('sgdev.migrate_manager');

    // Set migrate id list.
    $migrateManager->setMigrationIds(['news_node_csv_import']);

    $migrateManager->process();
```
