<?php

namespace Drupal\migrate_manager\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate_plus\Plugin\migrate\process\EntityGenerate;
use Drupal\paragraphs\Entity\Paragraph;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "paragraph_generate"
 * )
 *  Migrate config example :
 *  field_editorial_content:
 *    -
 *      plugin: paragraph_generate
 *      type: rich_text
 *      field_text/value: source_0
 *      field_text/format: full_html
 *      source:
 *        - body
 *
 */
class ParagraphGenerate extends EntityGenerate {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrateExecutable, Row $row, $destinationProperty) {
    $this->row = $row;
    $this->migrateExecutable = $migrateExecutable;
    return $this->generateEntity($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function generateEntity($value) {

    $fields = [];
    $configurations = $this->configuration;

    $fields['type'] = $configurations['type'];

    foreach ($this->configuration as $key => $config) {
      if (str_starts_with($key, 'field')) {
        $configArray = explode('/', $key);
        $fieldName = $configArray[0];
        $subFieldName = $configArray[1];
        $fieldValue = str_starts_with($config, 'source_') ? $value[explode('_', $config)[1]] : $config;
        $fields[$fieldName][$subFieldName] = $fieldValue;
      }
    }

    $paragraph = Paragraph::create($fields);
    $paragraph->save();
    $returnArray[] = ['target_id' => $paragraph->id(), 'target_revision_id' => $paragraph->getRevisionId()];
    return empty($returnArray) ? NULL : $returnArray;
  }

}
