<?php

namespace Drupal\migrate_manager\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Provides an Entity date formatter plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "entity_date_formatter"
 * )
 */
class EntityDateFormatter extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!empty($value)) {
      $timeStr = strtotime($value);
      return date("U", $timeStr);
    }
    return '';
  }
}

