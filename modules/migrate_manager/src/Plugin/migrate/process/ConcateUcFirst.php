<?php

namespace Drupal\migrate_manager\Plugin\migrate\process;

use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a concate_uc_first plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: concate_uc_first
 *     source: foo
 *     delimiter: "/"
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "concate_uc_first"
 * )
 *
 * @DCG
 * ContainerFactoryPluginInterface is optional here. If you have no need for
 * external services just remove it and all other stuff except transform()
 * method.
 */
class ConcateUcFirst extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      $delimiter = $this->configuration['delimiter'] ?? '';
      return ucfirst($value[0]) . $delimiter . ucfirst($value[1]);
    }
    else {
      throw new MigrateException(sprintf('%s is not an array', var_export($value, TRUE)));
    }
  }
}
