<?php

namespace Drupal\migrate_manager\Plugin\migrate\process;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Transliteration\TransliterationInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ephe_migrate\Manager\TermManager;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drush\Log\Logger;
use mysql_xdevapi\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a date_extract plugin.
 *
 * Usage:
 *
 * @code
 * process:
 *   bar:
 *     plugin: date_extract
 *     source: foo
 *     date_to_get : start
 * @endcode
 *
 * @MigrateProcessPlugin(
 *   id = "date_extract"
 * )
 *
 * @DCG
 * ContainerFactoryPluginInterface is optional here. If you have no need for
 * external services just remove it and all other stuff except transform()
 * method.
 */
class DateExtract extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  const FROM_FORMAT = 'd/m/Y';

  const TO_FORMAT = 'Y-m-d';

  const LOGGER_CHANNEL = 'Migrate Date extract';

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * DateExtract constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerChannel = $loggerChannelFactory->get(self::LOGGER_CHANNEL . ' (' . get_called_class() . ')');
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return \Drupal\ephe_migrate\Plugin\migrate\process\CourseDegreeLevel|static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('logger.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (!empty($value)) {

      $system_timezone = date_default_timezone_get();
      $default_timezone = !empty($system_timezone) ? $system_timezone : 'UTC';
      $to_timezone = $this->configuration['to_timezone'] ?? $default_timezone;

      // Extract date from string.
      preg_match_all('/[0-9]{1,2}\/[0-9]{2}\/[0-9]{4}/', $value, $matches);
      $date = $this->configuration['date_to_get'] == 'start' ? $matches[0][0] : $matches[0][1];
      $date = strlen(substr($date, 0, strpos($date, '/'))) == 1 ? '0' . $date : $date;

      if (!empty($date)) {
        try {
          $transformed = DateTimePlus::createFromFormat(self::FROM_FORMAT, $date)->format(self::TO_FORMAT, ['timezone' => $to_timezone]);
        }
        catch (\UnexpectedValueException $e) {
          $this->loggerChannel->error( $row->getSourceIdValues()['title'] . ' : ' . $e->getMessage());
          return NULL;
        }
        return $transformed;
      }
    }
    return NULL;
  }
}
