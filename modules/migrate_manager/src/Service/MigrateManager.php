<?php

namespace Drupal\migrate_manager\Service;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_tools\MigrateBatchExecutable;
use Drupal\migrate_tools\MigrateExecutable;

/**
 * Class MigrateManager
 *
 * @package Drupal\migrate_manager\Service
 */
class MigrateManager implements MigrateManagerInterface {

  const LOGGER_CHANNEL = 'Migrate Manager';

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerChannel;

  /**
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected MigrationPluginManagerInterface $migration;

  /**
   * @var array
   */
  protected array $migrateListIds;

  /**
   * ImportManager constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $migration
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory, MigrationPluginManagerInterface $migration) {
    $this->loggerChannel = $loggerChannelFactory->get(static::LOGGER_CHANNEL);
    $this->migration = $migration;
    $this->migrateListIds = [];
  }

  /**
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   * @param bool $batch
   *
   * @return bool
   */
  public function process(bool $update = TRUE, bool $sync = TRUE, int $limit = 0, bool $batch = TRUE): bool {
    try {
      foreach ($this->getMigrationIds() as $id) {
        // Execute migration.
        $this->runMigration($id, $update, $sync, $limit, $batch);
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
      return FALSE;
    }
    return TRUE;
  }

  /**
   * @return array
   */
  public function getMigrationIds(): array {
    return $this->migrateListIds;
  }

  /**
   * @param $migrationId
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   * @param bool $batch
   *
   * @return false|void
   */
  protected function runMigration($migrationId, bool $update = TRUE, bool $sync = TRUE, int $limit = 0, bool $batch = TRUE) {
    try {

      // Execute migration.
      /**
       * @var MigrationInterface $migrationInstance
       */
      $migrationInstance = $this->migration->createInstance($migrationId);

      if ($migrationInstance) {

        // Update existing entity imported.
        if ($update) {
          $migrationInstance->getIdMap()->prepareUpdate();
        }

        // Sync source
        if ($sync) {
          $migrationInstance->set('syncSource', TRUE);
        }

        // Options
        $options = [];
        // Set limit
        if ($limit) {
          $options['limit'] = $migrationInstance->getIdMap()->processedCount() + $limit;
        }

        // Run the migration.
        if ($batch) {
          $executable = new MigrateBatchExecutable($migrationInstance, new MigrateMessage(), $options);
          $executable->batchImport();
        }
        else {
          $executable = new MigrateExecutable($migrationInstance, new MigrateMessage(), $options);
          $executable->import();
        }

        // Reset status after processing.
        $migrationInstance->setStatus(MigrationInterface::STATUS_IDLE);
      }
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * @param array $ids
   *
   * @return void
   */
  public function setMigrationIds(array $ids) {
    $this->migrateListIds = $ids;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMigrationsLabel() {
    $output = [];
    foreach ($this->getMigrationIds() as $migrationId) {
      $output[$migrationId] = $this->migration->getDefinition($migrationId)['label'];
    }
    return $output;
  }

}
