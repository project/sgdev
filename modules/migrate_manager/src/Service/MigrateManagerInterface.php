<?php

namespace Drupal\migrate_manager\Service;


/**
 * Interface MigrateManagerInterface
 *
 * @package Drupal\migrate_manager\Service
 */
interface MigrateManagerInterface {

  /**
   * @param bool $update
   * @param bool $sync
   * @param int $limit
   * @param bool $batch
   *
   * @return bool
   */
  public function process(bool $update = TRUE, bool $sync = TRUE, int $limit = 0, bool $batch = TRUE): bool;

  /**
   * @return array
   */
  public function getMigrationIds(): array;

  /**
   * @param array $ids
   *
   * @return void
   */
  public function setMigrationIds(array $ids);
}
